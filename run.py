# -*- coding: utf8 -*-

import psutil
import json
import argparse
import multiprocessing
import subprocess
import time
import os
import traceback
from common.config.log import logger
from common.gracefulkiller import GracefulKiller
from common.config import config
from common.master import one_iter

redis_client = config['redis_client']
input_queue = config['INPUT_QUEUE']
killer = GracefulKiller()


def get_args():
	parser = argparse.ArgumentParser(description='爬虫登录')
	# 控制并发数
	parser.add_argument(
		'-b', dest='batch_size', type=int, required=False,
		default=1, help='输出集')
	
	args = parser.parse_args()
	return args


def get_pid(name, phone):
	'''
	 作用：根据进程名获取进程pid
	'''
	pids = psutil.process_iter()
	for pid in pids:
		try:
			pid_info = pid.cmdline()
		except:
			continue
		if len(pid_info) > 3 and 'python3' in pid_info[0]:
			if name in pid_info[1] and phone in ''.join(pid_info):
				result = pid.pid
				return result
	return None


def kill_process(require_fun, phone_number):
	try:
		aim_pid = get_pid(require_fun, phone_number)
		if aim_pid:
			logger.warning('kill pid : %s', aim_pid)
			os.system("kill -9 {}".format(aim_pid))
		else:
			logger.info("------没有目标进程-------")
	except:
		logger.error('---------kill进程发送错误：%s--------------', traceback.format_exc())


# 运行爬虫，
def spider(cmd_name):
	base_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(base_dir)
	# info = subprocess.Popen(shell=True, args=cmd_name,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	info = subprocess.Popen(shell=True, args=cmd_name)
	
	# 获取错误输出
	error_log = info.stderr.read().decode()
	# 获取正常输出
	normal_log = info.stdout.read().decode()
	if error_log:
		logger.error('----------脚本发生错误--------%s', error_log)
		return error_log
	else:
		logger.info('-----------正常返回----------%s', normal_log)
		return normal_log


# application_id
# ['{"18325279494":[{"id":"108","country":"+86","name":"111","phone":"18325279494","idnumber":"500240199711131076","password":"","twice_password":"","new_password":"Th1s#is#108","app_id":9,"nickname":"tuniu","app_name":"\\u9014\\u725b"}]}']

def start_login(login_list):
	pool_number = 20 if len(login_list) > 20 else len(login_list)
	pool = multiprocessing.Pool(processes=pool_number)
	require_fun = 'telegram_login'
	for login_info in login_list:
		print(login_info)
	
		phone_number = login_info.get('phone_number')
		login_info_str = '*'.join([k + '=' + str(v) for k, v in login_info.items()])
		cmd_name = "python ./telegram_login/start.py  -i {}".format(login_info_str)
		
		kill_process(require_fun, phone_number)
		pool.apply_async(spider, (cmd_name.strip(),))
	# flag_list.append(result.get())
	pool.close()
	# 等待所有子进程结束
	pool.join()
	# return flag_list
	return True


'''
{

​	phone: [

​		{

​			id: int //主键

​			person_id: int //重点人id - 类似Task中的target_id

		 	nickname: string  //参考applicaiton_template.php中的nickname
	
			password: string //密码  可能是空

​			twice_password: string // 二次验证的密码 - 可能是空

​		}

​	]

}

'''


def run(login_infos):
	errors = []
	origin_infos = [json.loads(x) for x in login_infos]
	logger.warning('-------------此次登录的app为---%s', origin_infos)
	exe_result = start_login(origin_infos)
	return (exe_result, errors)



if __name__ == '__main__':
	args = get_args()
	batch_size = args.batch_size
	in_queue = input_queue
	while True:
		try:
			if killer.is_killed:
				logger.info("received killed signal, exit now!")
				break
			# 启动入口
			x = one_iter(batch_size=batch_size, in_queue=in_queue, run=run)
			logger.warning('==================%s', x)
			if x and x[0]:
				pass
			else:
				logger.info('-------当前没有登录任务---------')
				time.sleep(5)
		except:
			logger.error('------发生全局错误---%s', traceback.format_exc())
			continue
	
	# update_status()
