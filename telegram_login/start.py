# -*- coding=utf-8 -*-
import argparse
import hashlib
import logging
import os
import sys
import time
import traceback


sys.path.append(os.path.dirname(__file__) + os.sep + '../')
from pyrogram import Client


from common.config import config
from common.master import get_login_info, login_result, delete_code
from common.gracefulkiller import GracefulKiller

killer = GracefulKiller()
logger = logging.getLogger(__file__)


api_id = config['api_id']
api_hash = config['api_hash']
telegram_session_path = config['telegram_session_path']
sockt_proxy = config["FOREIGN_PROXY_SOCKT"]
proxy = {"hostname": sockt_proxy.split(':')[0], "port": int(sockt_proxy.split(':')[1])}


def telegram_client_login(tel, mysql_id, twice_password, login_info):
	try:
		is_session_path =  tel + '.session'
		print(is_session_path)
		if os.path.exists(is_session_path):
			os.remove(is_session_path)
		delete_code(mysql_id)
		app = Client(tel, api_id=api_id, api_hash=api_hash, force_sms=True, workdir=telegram_session_path, proxy=proxy,
					 phone_number=tel,  password=twice_password, mysql_id=mysql_id)
		result = app.start()
		if isinstance(result, Client):
			user_id = app.get_me().id  # 1018491076
			id_add = 'telegram' + str(user_id)
			id_md5 = hashlib.md5(id_add.encode(encoding='UTF-8')).hexdigest()
			
			# 成功mongo存入telegram开发者
			current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
			
			
			# 将此开发者绑定的账号数联增加1
			
			# 成功推redis队列, 将配置改为我项目的配置
			
			login_result(mysql_id, '13', 'sucess', is_session_path )
		
		
			time.sleep(2)
			# 登录成功就删除提示短信
			# official_message = app.get_history(777000)
			# for messages_ids in official_message:
			# 	official_messages_id = messages_ids.message_id
			# 	app.delete_messages(777000, official_messages_id, revoke=False)
			app.stop()
			logger.info('----------------完成登录------------关闭telegram会话')
		 
		else:
			messages = 'telegram连接失败'
		
			logger.warning('---telegram连接失败-------------%s', result)
		
			login_result(mysql_id, '19', messages)
	
	except Exception as  e:
		messages = 'telegram由于异常原因,登录出现未知错误请排查程序结束'
	
		if 'The confirmation code is invalid' in repr(e):
			messages = "短信验证码错误"
		
		login_result(mysql_id, '19', messages)
		print('telegram由于异常原因,登录出现未知错误请排查程序结束', traceback.format_exc())


if __name__ == '__main__':
	info_dict = get_login_info()
	phone_number_one = info_dict.get('phone')
	_id = info_dict.get('id')
	country = info_dict.get('country')
	phone_number = country + phone_number_one
	twice_password = info_dict.get('twice_password')
	try:
		telegram_client_login(phone_number, _id, twice_password, info_dict)
	except Exception:
		traceback.print_exc()
		logger.warning('telegram由于异常原因,登录出现未知错误请排查程序结束 %s', traceback.format_exc())


