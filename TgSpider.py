import yaml
import traceback
import argparse
import asyncio
import json
from telegram_login.pyrogram.types import InputPhoneContact
from telegram_login.pyrogram import Client


# from pyrogram.api import functions
import logging as logger
config = yaml.load(open('./config/base.yaml', encoding='utf-8'), Loader=yaml.SafeLoader)
print(config)
api_id = config['api_id']
api_hash = config['api_hash']
sniffer_session = config['sniffer_session']
sockt_proxy = config['FOREIGN_PROXY_SOCKT']
telegram_session_path = config['telegram_session_path']
proxies = {"hostname": sockt_proxy.split(':')[0], "port":  int(sockt_proxy.split(':')[1])}

'''注册，登录，加群，邀请进群，查询，五个功能， 然后写成api 你可以直接用http协议调用  '''
class TgSpider():
	def __init__(self):
		self.app = ''
	def init_session(self, phone):
		try:
			app =  Client(f"+{phone}",api_id, api_hash,  workdir=telegram_session_path, proxy=proxies )
			app.start()
			self.app = app
			return app
		except:
			print('------初始化session失败-----')
		
			return None
	
	
	def get_me(self):
		result = self.app.get_me()
		print(result)
	
	
	def join_group(self, group_link='https://t.me/+dZwI5F1zxJBjZTZl'):
		try:
			join_flag  = self.app.join_chat(group_link)
		except:
			if 'he user is already a participant of ' in traceback.format_exc():
				self.app.stop()
	
	
	
	def invite_friend(self, friend_firstname, group_firstname):
		try:
			results = self.app.get_dialogs()
			group_id, friend_id = None, None
			for dialog in results:
				if dialog.chat.first_name == group_firstname:
					group_id = dialog.chat.id
				if dialog.chat.first_name == friend_firstname:
					friend_id = dialog.chat.id
					print('获取到邀请人的id: ', friend_id)
			if group_id and friend_id:
				try:
					print("开始邀请： ")
					result1 =  self.app.add_chat_members(group_id, friend_id)
					return f'-邀请{friend_firstname} 加入{group_firstname}成功'
				except:
					return '-邀请成功'
			else:
				return f"未找到相应得群id:{group_id}---或者是朋友id{friend_id}"
		except:
			return  f"邀请朋友入群发生错误 error: {traceback.format_exc()}"
	
	def stop_session(self):
		self.app.stop()
		
	def sniffer(self, phone):
		try:
			sniffer_app = Client(f'+{sniffer_session}',   workdir=telegram_session_path, api_id=int(api_id), 	api_hash=api_hash, proxy=proxies)
			sniffer_app.start()
		except:
			return f"嗅探会话初始化失败 error: {traceback.format_exc()}"
		else:
			logger.info('-----------嗅探会话初始化成功------------------')
			res = sniffer_app.import_contacts([InputPhoneContact(phone, phone)])
			user = res.users
			if user:
				user_id = user[0].id
				flag = '已经注册'
				sniffer_app.delete_contacts([user_id])  # 因为采用添加的方式,如果账号存在,添加成为联系,然后在删除联系人
			else:
				flag = '未注册'
			print(flag)
			sniffer_app.stop()
			return flag
	def get_contacts(self):
		try:
			contact_list = self.app.get_contacts()
			user_list = []
			for user in contact_list:
				user_dict = {}
				user_dict['id'] = user.id
				user_dict['first_name'] = user.first_name
				user_dict['last_name'] = user.last_name
				user_dict['last_online_date'] = user.last_online_date
				user_list.append(user_dict)
	
	
	
			return json.dumps(user_list, ensure_ascii=False)
		except:
			return  f'获取联系人发生错误， error: {traceback.format_exc()}'
	def set_pwassword(self, password):
		try:
			flag = self.app.enable_cloud_password( password=password)
		except:
			if 'There is already a cloud password enabled' in traceback.format_exc():
				return '该账号已经被设置了二次密码'
			return f'设置二次密码，发生错误 error: {traceback.format_exc()}'
		else:
			return "设置二次密码成功"
	def register_account(self, phone):
		
		
		
		
		register_app = Client(f'+{phone}',   workdir=telegram_session_path, api_id=int(api_id), 	api_hash=api_hash, proxy=proxies, phone_number=phone, force_sms=True)
		register_app.connect()
		sent_code = register_app.send_code(phone_number=phone)
		signed_up = register_app.sign_up(phone, sent_code, first_name="TGsd")
	
		first_name = 'in'
		last_name = 'guo'
		# sent_code = register_app.resend_code(self.phone_number, sent_code.phone_code_hash)
		#
		# register_app.sign_up(phone,first_name=first_name, last_name=last_name )


if __name__=='__main__':
	test = TgSpider()
	test.register_account('8615228294620')
	# loop = asyncio.get_event_loop()
	# task = asyncio.ensure_future(test.register_account('8615228294620'))
	# loop.run_until_complete(task)
	
	# parser = argparse.ArgumentParser(description='爬虫接口登陆')
	# # INPUT_QUEUE, 读取下载队列里面的ｕｒｌ＇
	# parser.add_argument(
	# 	'-p', dest='phone_number', type=str, required=False,
	# 	default='+8615228994667', help='手机号')
	#
	# args = parser.parse_args()
	# phone_number = args.phone_number
	# test = TgSpider()
	# loop = asyncio.get_event_loop()
	# loop.run_until_complete(asyncio.ensure_future(test.login(phone_number)))
	# test.login(phone_number)
