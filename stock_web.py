# Name: WebSocket Server
# Date: 2022-08-03

import os
import datetime
import json
import asyncio
import psutil
import socket
import websockets
import traceback

import yaml
from pyrogram import Client
from pyrogram.types import InputPhoneContact

# from pyrogram.api import functions
import logging as logger
from spider import TgSpider

config = yaml.load(open('./config/base.yaml', encoding='utf-8'), Loader=yaml.SafeLoader)
print(config)
api_id = config['api_id']
api_hash = config['api_hash']
sniffer_session = config['sniffer_session']
sockt_proxy = config['sockt_proxy']

# 主页面
app_dict = {}
#定义一个webstocket
async def echo(websocket, path):
	async for message in websocket:
		message_dict = json.loads(message)
		print('接受到信息', message_dict)
		if message_dict.get('msg_type')=='login':
			phone = message_dict.get('phone')
			test = TgSpider()
			app = await test.init_session(phone)
		
			app_dict[phone] = app
		
			await websocket.send(f"{phone}--->等待触发短信")

		elif message_dict.get('msg_type')=='send_code':
			phone = message_dict.get('phone')
			now_app = app_dict.get(phone)
			if now_app:
				await websocket.send(f"{phone}--->已触发短信")
			
				await now_app.start()
				print('----发送')
		
			else:
				print(f'没有初始化{phone}会话')
		elif message_dict.get('msg_type')=='inupt_code':
			phone = message_dict.get('phone')
			sms_code = message_dict.get('sms_code')
			now_app = app_dict.get(phone)
			now_app.phone_code = sms_code
		else:
			await websocket.send(json.dumps({"msg_type":'参数错误'}))
	await asyncio.sleep(1)
# --------------- 主页面通信接口 --------------- #


#@KecsPaTyY3QYWUH


if __name__ == '__main__':
	
	hostip = socket.gethostbyname(socket.gethostname())
	print(hostip)
	pyname = os.path.basename(__file__)
	login_token = ''
	port = 8765
	loop = asyncio.get_event_loop()
	loop.run_until_complete(websockets.serve(echo, hostip, port))
	asyncio.get_event_loop().run_forever()



