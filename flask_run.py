# -*- coding: utf-8 -*-
import pymysql
import traceback
import os
import yaml
from  TgSpider import TgSpider
import logging as logger
from flask import Flask,  request
app = Flask(__name__)

def readcfg(yaml_name):
	base_dir = os.path.dirname(os.path.abspath(__file__))
	path_dir = base_dir + '/config/{}'.format(yaml_name)
	db_info = yaml.load(open(path_dir, encoding='utf-8'), Loader=yaml.FullLoader)
	return db_info


dbconfig = readcfg('production.yaml')

MYSQL_INFO = dbconfig['MYSQL_INFO']
# 获取mysql连接
def connect_mydb():
	db = pymysql.connect(host=MYSQL_INFO['host'], user=MYSQL_INFO['user'],
						 passwd=MYSQL_INFO['passwd'], db=MYSQL_INFO['db'], port=MYSQL_INFO['port'],
						 charset='utf8', autocommit=True)
	return db
@app.route('/login/<phone>')
def login_inint(phone):
	return phone

@app.route('/login_result', methods=['POST'])
def login_result():
	cur = db.cursor()
	try:
		if request.method == 'POST':
			mysql_id = request.form['mysql_id']
			status = request.form['status']
			message = request.form['message']
			session_path = request.form['session_path']
			sql = 'update tg_account set status="%s",  message="%s", session_path="%s" where id=%s' % (status,message, session_path, mysql_id )
			cur.execute(sql)
			db.commit()
	except:
		response = traceback.format_exc()
	else:
		response = 'sucess'
	cur.close()
	return response
@app.route('/delete_code/<mysql_id>')
def delete_code(mysql_id):
	cur = db.cursor()
	try:
		sql = 'update tg_account set sms_code="" where id=%s' % (mysql_id)
		cur.execute(sql)
		cur.execute(sql)
		db.commit()
	except:
		response = traceback.format_exc()
	else:
		response = 'sucess'
	
	cur.close()
	return response
@app.route('/login_code/<mysql_id>')
def login_smscode(mysql_id):
	cur = db.cursor()
	try:
		sql = 'select sms_code from tg_account where id =%s'%(mysql_id)
		cur.execute(sql)
		result = cur.fetchone()
		if result:
			code = result[0]
			if code:
				response =  code
			else:
				response = ''
		else:
			response = ''
	except:
		response = traceback.format_exc()
	cur.close()
	return response
def juge_session(session_phone):
	cur = db.cursor()

	sql = 'select status from tg_account where phone =%s'%(session_phone)
	cur.execute(sql)
	result = cur.fetchone()
	cur.close()
	if result:
		status = result[0]
		if status =='13':
			return True
		else:
			return False
	else:
		return False
@app.route('/join_group/', methods=['POST'])
def join_group():
	if request.method == 'POST':
		group_link = request.form['group_link']
		session_phone = request.form['session_phone']
		city = request.form['city']
		session_flag = juge_session(session_phone)
		if session_flag:
			test = TgSpider()
			app = test.init_session(city+session_phone)
			if app:
				test.join_group(group_link)
				return f'用户{session_phone}， 成功加入{group_link}'
			else:
				return f"{session_phone}-session不可用"
		else:
			return f'当前账号{session_phone}未登录'




@app.route('/invite_friend', methods=['POST'])
def invite_friend():
	if request.method == 'POST':
		friend_firstname = request.form['friend_firstname']
		group_firstname = request.form['group_firstname']
		session_phone = request.form['session_phone']
		city = request.form['city']
		session_flag = juge_session(session_phone)
		if session_flag:
			test = TgSpider()
			app = test.init_session(city+session_phone)
			if app:
				response = test.invite_friend(friend_firstname, group_firstname)
				app.stop()
				return response
			else:
				return f"{session_phone}-session不可用"
		else:
			return f'当前账号{session_phone}未登录'
	

@app.route('/sniffer/<phone>')
def sniffer(phone):
	test = TgSpider()
	response = test.sniffer(phone)
	return response


@app.route('/get_contacts/<phone>/<city>')
def get_contacts(phone, city):
	session_flag = juge_session(phone)
	if session_flag:
		test = TgSpider()
		app = test.init_session(city+phone)
		if app:
			response = test.get_contacts()
			app.stop()
			return response
		else:
			return f"{phone}-session不可用"
	else:
		return f'当前账号{phone}未登录'





@app.route('/set_pwassword/',  methods=['POST'])
def set_pwassword():
	if request.method == 'POST':
		session_phone = request.form['session_phone']
		password = request.form['password']
		city = request.form['city']
		session_flag = juge_session(session_phone)
		if session_flag:
			test = TgSpider()
			app = test.init_session(city+session_phone)
			if app:
				response = test.set_pwassword(password)
				app.stop()
				return response
			else:
				return f"{session_phone}-session不可用"
		else:
			return f'当前账号{session_phone}未登录'
'''注册，登录，加群，邀请进群，嗅探，设置二次登录密码， 查询联系人五个功能， 然后写成api 你可以直接用http协议调用  '''


if __name__ == '__main__':
	db = connect_mydb()
	
	app.run()
