#!/usr/bin/python
# -*- coding: utf8 -*-
import logging
import signal

logger = logging.getLogger(__file__)

class GracefulKiller:
    """接收信号量，设置退出状态
    """
    is_killed = False

    def __init__(self, signals=None):
        """注册接收的信号量，默认为 SIGNINT, SIGTERM
        signals list
        """
        if not signals:
            signals = [signal.SIGINT, signal.SIGTERM]

        for _signal in signals:
            signal.signal(_signal, self.signal_handler)
            signal.signal(_signal, self.signal_handler)

    def signal_handler(self, signum, frame):
        logger.info("received signal:%s, set is_killed to True", signum)
        self.is_killed = True
