# -*- coding: utf8 -*-
import os
import requests
import traceback
import json
import time
import argparse
import base64
from hashlib import md5
from random import choice
from .config.log import logger
from .config import config
from .gracefulkiller import GracefulKiller
from time import sleep



redis_client = config['redis_client']
killer = GracefulKiller()

def get_login_info():
    parser = argparse.ArgumentParser(description='爬虫接口登陆')
    # INPUT_QUEUE, 读取下载队列里面的ｕｒｌ＇
    parser.add_argument(
        '-p', dest='phone_number', type=str, required=False,
        default='+8613601076164', help='手机号')
    parser.add_argument(
        '-i', dest='login_info', type=str, required=False,
        default='test', help='记录id')
    args = parser.parse_args()
    login_info = args.login_info
    info_dict = {i.split('=')[0]: i.split('=')[1] for i in login_info.split('*')}
    return info_dict


def add_forieg_proxy(ip_type=None):
    # return None
    #默认为国内代理
    if ip_type:
        queque_name = "fip_queques"
    else:
        queque_name = "dip_queques"
    ips = redis_client.get(queque_name)
    if ips:
        if json.loads(ips.decode()):
            ip_list = ['{}:{}'.format(i.get('ip'), i.get('port')) for i in json.loads(ips.decode())]
            return choice(ip_list)
        else:
            return None
    else:
        return None





#格式化selenium的cookie为标准字符串
def format_cookie(cookie_list):
    cookies_str = ''
    for i in cookie_list:
        cookie_name = i.get('name')
        cookie_value  = i.get('value')
        cookies_str += cookie_name + '=' + cookie_value + ';'
    return cookies_str


def get_api(api_type, id):
    base_url = config['api_url'] + api_type + '/' +str(id)
    res = requests.get(url=base_url, verify=False)
    return res.text

def post_api(api_type, id):
    base_url = config['api_url'] + api_type + '/' +str(id)
    res = requests.get(url=base_url, verify=False)
    return res.text

def login_result(_id, status, message, session_path=''):
    base_url = config['api_url'] + 'login_result'
    data = {'mysql_id': _id, 'status': status, 'message':message, 'session_path':session_path }
    
    response = requests.post(url=base_url, data=data )
    if response.text == 'sucess':
        logger.warning('-------成功更新登录状态-------', )
    else:
        logger.warning('------更新登录状态报错 %s---------------', response.text)


def delete_code(_id):
    result = get_api(api_type='delete_code', id=_id)
    if result == 'sucess':
        logger.warning(f'-------成功删除id为：{_id}短信-------', )
    else:
        logger.warning(f'------删除短信发生错误 {result}---------------')



#获取短信验证码
def get_code(_id, nickname='telegram'):
    #默认获取10次
    for i in range(0,6):
        if killer.is_killed:
            logger.info("received killed signal, exit now!")
            break
        code = get_api(api_type='login_code', id=_id)

        if code:
            logger.warning('-------%s获取到验证码code:%s-------', nickname, code)
            return code
        else:
            #当没有获取到验证码并且被spider_status=10, 并且出现这样的情况两次, 则退出循环
            logger.warning('-------%s没有获取到验证码-------', nickname)
            time.sleep(30)
    #如果若干秒后都没有更新验证码,则判定为失败
    logger.warning('------当前登陆应用（%s）获取验证码超时---------------', nickname)
    return False


# 需要重新上传二次验证密码
def send_passed_twice(_id):
    res_json = get_api_info(api_type='twice_pwd_result', id=_id)
    if res_json.get('success'):
        logger.warning('--------回写触发二次验证密码通知成功，------------')
        return True
    else:
        logger.warning('--------回写触发二次验证密码通知， 该应用状态并未登录执行中"------------%s', res_json)
    return False


# 重新获取二次验证密码
def get_pwd_twice(_id, nickname):
    # 默认获取10次
    res_json = get_api_info(api_type='get_twice_pwd', id=_id)
    twice_code = res_json.get('data')
    if twice_code:
        get_twice_code = twice_code.get('twice_password')
        if not res_json.get('success'):
            logger.warning('----------该应用状态并未登录执行中-------------')
            return False
        if get_twice_code:
            logger.warning('-------%s重新获取到二次验证密码twice_code:%s-------', nickname, get_twice_code)
            return get_twice_code
        else:
            # 当没有获取到验证码并且被spider_status=10, 并且出现这样的情况两次, 则退出循环
            logger.warning('-------%s没有重新获取到二次验证密码-------', nickname)
    # 如果若干秒后都没有更新验证码,则判定为失败
    logger.warning('------当前登陆应用（%s）未获取到二次验证密码---------------', nickname)
    return False



#获取当前时间
def get_current_time():
    current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
    return current_time




#从redis队列取任务
def one_iter(batch_size, in_queue, run, encoding='utf8'):
    
    with redis_client.pipeline(transaction=False) as p:
        for _ in range(batch_size):
            p.lpop(in_queue)
        in_strs = p.execute()
    if encoding:
        in_strs = [x.decode() for x in in_strs if x]
    else:
        in_strs = [x for x in in_strs if x]
    if not in_strs:
        sleep(3)
        return
    try:
        results, errors = run(in_strs)
    except Exception as e:
        # redis_client.rpush(in_queue, *in_strs)
        logger.error('Exception: %s, %s', e,
                     traceback.format_exc())
        return
    
    if errors:
        if encoding:
            pass
            # redis_client.rpush(in_queue, *[json.dumps(x) for x in errors])
        else:
            pass
            # redis_client.rpush(in_queue, *errors)
    return results, errors






