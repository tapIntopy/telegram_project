#!/usr/bin/python
# -*- coding: utf8 -*-
import os
import yaml


try:
	import pymysql
	from DBUtils.PooledDB import PooledDB

except ImportError:
	pymysql = None

try:
	from redis import StrictRedis
except ImportError:
	StrictRedis = None
try:
	import sentry_sdk
except ImportError:
	sentry_sdk = None

try:
	FileNotFoundError()
except:
	FileNotFoundError = Exception


class SingalConfig():
	def __new__(cls, *args, **kwargs):
		if not hasattr(SingalConfig, '_instance'):
			SingalConfig._instance = object.__new__(cls)
		return SingalConfig._instance
	
	def _get_settings(self, base_dir, relative_path):
		path = os.path.join(base_dir, relative_path)
		if not os.path.isfile(path):
			raise FileNotFoundError('没有 %s' % relative_path)
		with open(path, encoding='utf-8') as file:
			return yaml.load(file, Loader=yaml.SafeLoader)
	
	def get_config(self, base_dir):
		config = self._get_settings(base_dir, 'config/base.yaml')
		config.update(self._get_settings(base_dir, 'config/production.yaml'))
	
		config['redis_client'] = StrictRedis.from_url(config['REDIS_URL'])
		return config
config_obj = SingalConfig()

config = config_obj.get_config(os.path.dirname(os.path.dirname(
	os.path.dirname(os.path.abspath(__file__)))))# -*- coding: utf-8 -*-
