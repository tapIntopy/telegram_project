import httpx
import requests


headers = {
    "Host": "p.t9live.vip",
    "pragma": "no-cache",
    "cache-control": "no-cache",
    "sec-ch-ua": "\"Chromium\";v=\"106\", \"Google Chrome\";v=\"106\", \"Not;A=Brand\";v=\"99\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"macOS\"",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "sec-fetch-site": "none",
    "sec-fetch-mode": "navigate",
    "sec-fetch-user": "?1",
    "sec-fetch-dest": "document",
    "accept-language": "zh-CN,zh;q=0.9,en;q=0.8"
}
url = "https://p.t9live.vip/index.html?language=tw&platform=PC&mask=false"
# response = requests.options(url, headers=headers)
#
# print(response.text)
# print(response)
proxies = {
    'http://': "http://127.0.0.1:8888",
    'https://': "http://127.0.0.1:8888",
}
# with httpx.Client(http2=True, proxies=proxies, verify=False) as client:
with httpx.Client(http2=True) as client:
    response = client.get(url, headers=headers)
    print(response.status_code)
    print(response.text)